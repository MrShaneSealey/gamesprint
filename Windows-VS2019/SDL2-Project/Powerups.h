#ifndef POWERUPS_H_
#define POWERUPS_H_

#include "SDL2Common.h"
#include "Sprite.h"

#include <string>
using namespace std;

class Vector2f;
class Sprite; 
class Game;

class Powerups : public Sprite
{
private:

	int state;

	//sprite info
	static const int SPRITE_WIDTH = 16;
	static const int SPRITE_HEIGHT = 16;

	int health;

	//game
	Game* game;

	int points;

public:
	Powerups();
	~Powerups();

	//dead state is temp, intil i can get the player to collide with it.
	bool isDead();

	void init(SDL_Renderer* renderer);
	void update(float timeDeltaInSeconds);

	enum PwrState{ADD, DEAD};
	
	int getCurrentAnimationState();
	
	int getPoints();
	void setPoints(int pointValue);

	void takeDamage(int damage);

	void respawn(const int MAX_HEIGHT, const int MAX_WIDTH);

	//draw
	void setGame(Game* game);
};

#endif // !POWERUPS_H_
