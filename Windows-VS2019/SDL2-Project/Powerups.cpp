#include "Powerups.h"
#include "Animation.h"
#include "Game.h"
#include "Vector2f.h"
#include "AABB.h"
#include "TextureUtils.h"
#include "Sprite.h"

#include <iostream>


Powerups::Powerups() : Sprite()
{
	state = ADD;

	targetRectangle.w = SPRITE_WIDTH;
	targetRectangle.h = SPRITE_HEIGHT;

	health = 2;

	points = 5;
}

void Powerups::init(SDL_Renderer* renderer)
{
	string path("assets/images/Powerups.png");

	Vector2f position(210.0f, 210.0f);

	Sprite::init(renderer, path, 2, &position);

	animations[ADD]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
	animations[DEAD]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);

	animations[DEAD]->setLoop(false);

	for (int i = 0; i < maxAnimations; i++)
	{
		animations[i]->setMaxFrameTime(0.2f);
	}

	aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);

}

void Powerups::update(float timeDeltaInSeconds)
{
	Sprite::update(timeDeltaInSeconds);
}

int Powerups::getCurrentAnimationState()
{
	return state;
}

void Powerups::setGame(Game *game)
{
	this->game = game;
}

Powerups::~Powerups()
{

}

bool Powerups::isDead()
{
	if (health > 0)
		return false;
	else
		return true;
}

void Powerups::respawn(const int MAX_HEIGHT, const int MAX_WIDTH)
{
	health = 100;

	Vector2f randomPostion;

	int doubleWidth = MAX_WIDTH;
	int doubleHeight = MAX_HEIGHT;
	// get a random number between 0 and 
	// 2 x screen size. 
	int xCoord = rand() % doubleWidth;
	int yCoord = rand() % doubleHeight;

	// if its on screen move it off. 
	if (xCoord < MAX_WIDTH)
		xCoord *= -1;

	if (yCoord < MAX_HEIGHT)
		yCoord *= -1;

	randomPostion.setX(xCoord);
	randomPostion.setY(yCoord);

	this->setPosition(&randomPostion);
}

int Powerups::getPoints()
{
	return points;
}

void Powerups::setPoints(int pointValue)
{
	points = pointValue;
}

void Powerups::takeDamage(int damage)
{
	health -= damage;

	if (!(health > 0))
		state = DEAD;
}